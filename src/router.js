import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'
import React from 'react'

import App from "./App";
import Success from "./Success"
import Upload from './Upload'
export const routing = (
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={App} />
            <Route path="/upload" component={Upload} />
            <Route path="/success" component={Success} />
            <Redirect to="/" />
        </Switch>
    </BrowserRouter>
)