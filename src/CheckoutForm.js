import React, { Component } from 'react';
import { CardElement, injectStripe } from 'react-stripe-elements';
import { MDBBtn } from 'mdbreact'
import axios from 'axios'
class CheckoutForm extends Component {
    constructor(props) {
        super(props);
        this.submit = this.submit.bind(this);
        this.checkOut = this.checkOut.bind(this);
    }
    async checkOut(ev) {
        axios.post('http://localhost:8002/checkout', {})
            .then(async (res) => {
                localStorage.setItem('res', JSON.stringify(res))
                const { error } = await this.props.stripe.redirectToCheckout({
                    // Make the id field from the Checkout Session creation API response
                    // available to this file, so you can provide it as parameter here
                    // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
                    sessionId: res.data.id
                })
            })
            .catch(err => {
                console.error(err)
            })
    }
    async submit(ev) {
        this.props.stripe.createToken({ name: "Name" })
            .then(res => {
                let payload = {
                    stripeToken: res.token.id
                }
                axios.post('http://localhost:8002/charge', payload)
                    .then(res => {
                        if (res.ok) console.log("Purchase Complete!")
                    })
                    .catch(err => {
                        console.error(err)
                    })
            })
            .catch(err => {
                console.error(err)
            })
    }

    render() {
        return (
            <div className="checkout p-4 m-3">
                <p>Would you like to complete the purchase?</p>
                <CardElement />
                <MDBBtn className="mt-4" outline onClick={this.submit}>Purchase</MDBBtn>
                <MDBBtn className="mt-4" outline onClick={this.checkOut}>CheckOut</MDBBtn>
            </div>
        );
    }
}

export default injectStripe(CheckoutForm);