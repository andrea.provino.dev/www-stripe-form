import React from "react"
import queryString from "query-string"
class Success extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            params: {}
        }
    }
    async componentDidMount() {
        // get query string params as object
        const params = queryString.parse(this.props.location.search)
        await this.setState({ ...this.state, "params": params })
        console.log(this.state)
    }
    render() {
        const params = this.state.params
        return (
            <div>
                {this.state.params.session_id}
            </div>
        )
    }
}

export default Success