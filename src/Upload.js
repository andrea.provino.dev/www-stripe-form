import React from 'react';
import { MDBRow, MDBCol, MDBCard } from 'mdbreact';
import axios from 'axios';
class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedFile: null
        }
        this.onChangeHandler.bind(this)
    }
    onChangeHandler = (event) => {
        this.setState({
            ...this.state,
            selectedFile: event.target.files,
        })
    }
    onClickHandler = (event) => {
        const data = new FormData()
        for (var x = 0; x < this.state.selectedFile.length; x++) {
            data.append('file', this.state.selectedFile[x])
        }
        axios.post("http://localhost:8002/uploadfile", data, { // receive two parameter endpoint url ,form data 
        })
            .then(res => { // then print response status
                console.log(res.statusText)
            })
            .catch(err => {
                console.log(err)
            })
    }
    render() {
        return (
            <div className="App">
                <MDBCard className="z-depth-2">
                    <h1>Upload File Form</h1>
                    <input multiple type="file" name="file" onChange={this.onChangeHandler} />
                    <button type="button" className="btn btn-success" onClick={this.onClickHandler}>Upload</button>
                </MDBCard>

            </div>
        );
    }

}

export default App;
