import React from 'react';
import { Elements, StripeProvider } from 'react-stripe-elements';
import CheckoutForm from './CheckoutForm';
import { MDBRow, MDBCol, MDBCard } from 'mdbreact';
import './App.css';
const stripe = {
  private_key: process.env.REACT_APP_PRIVATE_KEY,
  public_key: process.env.REACT_APP_PUBLIC_KEY
}
class App extends React.Component {
  componentDidMount() {
  }
  render() {
    return (
      <div className="App">
        <MDBCard className="z-depth-2">
          <h1>Payment Form</h1>
          <StripeProvider apiKey={stripe.public_key}>
            <div className="example">
              <Elements>
                <CheckoutForm />
              </Elements>
            </div>
          </StripeProvider>
        </MDBCard>

      </div>
    );
  }

}

export default App;
